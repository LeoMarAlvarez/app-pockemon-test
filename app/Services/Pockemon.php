<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Pockemon {
    protected $url;

    public function __construct() {
        $this->url = "https://pokeapi.co/api/v2/pokemon";
    }

    private function request($link)
    {
        try {
            $response = Http::get($link);

            if ($response->ok()) {
                return json_decode(json_encode($response->json()));
            } else {
                throw new \Exception($response->status() . ": " . $response->body());
            }
        } catch (RequestException $th) {
            return $th->getMessage();
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function getPockemons($url = "") :array
    {
        $pockemons = Array();
        $response = $this->request($this->url . "?" . $url);
        if (is_array($response->results)) {
            foreach ($response->results as $element) {
                $pockemon = $this->getPockemon($element->url);
                $pockemons[] = $this->getProperties($pockemon);
            }
        }

        return [
            "next" => Str::after($response->next, "?"),
            "previous" => Str::after($response->previous, "?"),
            "pockemons" => $pockemons
        ];
    }

    public function getPockemonByName($name) :array
    {
        return $this->getProperties((array) $this->request($this->url . "/" . $name));
    }

    private function getPockemon($url)
    {
        return (array) $this->request($url);
    }

    private function getProperties($pockemon) :array
    {
        $pockemon['sprites']->other = (array) $pockemon['sprites']->other;
        return [
            "name" => Arr::get($pockemon, 'name', "no-name"),
            "abilities" => Arr::pluck((array) $pockemon['abilities'], 'ability.name'),
            "types" => Arr::pluck((array) $pockemon['types'], 'type.name'),
            "image" => Arr::get((array) $pockemon['sprites']->other['official-artwork'], 'front_default', '')
        ];
    }
}