<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Pockemon;

class PockemonController extends Controller
{
    protected $pockemonService;

    public function __construct(Pockemon $pockemon)
    {
        $this->pockemonService = $pockemon;
    }

    public function getPockemons(Request $request)
    {
        return response()->json(
            $this->pockemonService->getPockemons("offset={$request->offset}&limit={$request->limit}")
        );
    }

    public function getPockemon(Request $request)
    {
        return response()->json(
            $this->pockemonService->getPockemonByName($request->name)
        );
    }
}
