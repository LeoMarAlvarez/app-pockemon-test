# App Pokemon find

## Requisitos para la instalación 
- "php": "^7.3|^8.0"
ó
- docker & docker-compose

## Instalación

- Descargar el proyecto

- Desde la consola
    * acceder al directorio raíz
    - Opción 1    
        * ejecutar:
            - composer install
            - php artisan key:generate
            - php artisan serve
            - acceder desde un navegador a http://localhost:8000
    - Opcion 2
        * ejecutar
            - docker-compose up -d
            - docker exec -it app-pockemon-test_myapp_1 composer install
            - docker exec -it app-pockemon-test_myapp_1 php artisan key:generate
            - acceder desde un navegador a http://localhost:3000
