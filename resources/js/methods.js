import { trim } from "lodash";

const { default: axios } = require("axios");

const getPockemons = async(link = "offset=0&limit=10") => {
    charged(false)
    await axios.get("/api/pockemon-list?" + link)
        .catch(err => {
            let div = document.createElement("div")
            div.classList = "badge badge-warning"
            div.innerHTML = `<h1>${err.data}</h1>`
        }).then(resp => {
            resp = resp.data
            charged(true)
            if (resp.next || resp.previous) {
                document.querySelector("#buttons").classList.remove('d-none')
                let sig = document.querySelector("#buttons").children[1]
                let ant = document.querySelector("#buttons").children[0]
                if (resp.next) {
                    sig.classList.remove("disabled")
                    sig.setAttribute('data-search', resp.next)
                } else {
                    sig.classList.add("disabled")
                }
                if (resp.previous) {
                    ant.classList.remove("disabled")
                    ant.setAttribute('data-search', resp.previous)
                } else {
                    ant.classList.add("disabled")
                }
            } else {
                document.querySelector("#buttons").classList.add('d-none')
                sig.classList.add('disabled')
                ant.classList.add('disabled')
            }
            document.querySelector("#list").innerHTML = ""
            resp.pockemons.map(e => {
                document.querySelector("#list").append(
                    makeCard(e)
                )
            })
        })
}

document.querySelector("#form-search").addEventListener("submit", (form) => {
    form.preventDefault()
    charged(false)
    let search = document.querySelector("#search")
    if (!trim(search.value)) {
        getPockemons()
    } else {
        axios.get(`/api/pockemon?name=${search.value}`)
            .catch(err => console.error)
            .then(resp => {
                charged(true)
                document.querySelector("#buttons").classList.add('d-none')
                document.querySelector("#list").innerHTML = ""
                document.querySelector("#list").append(
                    makeCard(resp.data)
                )
            })
    }
})

const makeCard = (pockemon) => {
    let card = document.createElement("div");
    card.classList = 'card mb-3'
    card.style.maxWidth = "580px"
    card.innerHTML = `
    <div class="row g-0">
        <div class="col-4">
            <img src="${pockemon.image}" class="img-fluid rounded-start" alt="${pockemon.name}">
        </div>
        <div class="col-8">
            <div class="card-body">
                <h3 class="card-title">${pockemon.name}</h3>
                <div class="card-text">
                    <h5>Habilididades</h5>
                    <p class="fw-bold">${pockemon.abilities.toString()}</p>
                </div>
                <div class="card-text">
                    <h5>Tipos</h5>
                    <p class="fw-bold">${pockemon.types.toString()}</p>
                </div>
            </div>
        </div>
    </div>
    `
    return card
}

const charged = (bandera) => {
    if (bandera) {
        document.querySelector("#list").classList.remove("d-none")
        document.querySelector("#spinner").classList.add("d-none")
        document.querySelector("#title").classList.remove('d-none')
    } else {
        document.querySelector("#list").classList.add("d-none")
        document.querySelector("#title").classList.add('d-none')
        document.querySelector("#spinner").classList.remove("d-none")
    }
}

export { getPockemons };