<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>App Pokemon Test</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div id="app" class="container-fluid justify-content-center align-items-center">
            <div class="text-center">
                <h1 class="h1">Pokemon Finder</h1>
                <span class="text-muted">El que quiere pokemons, que los busque.</span>
            </div>
            <form id="form-search" class="justify-content-center align-items-center">
                <div class="row justify-content-center align-items-center">
                    <div class="col-6 form-group">
                        <input 
                            type="text" 
                            name="search" 
                            id="search" 
                            class="form-control"
                            placeholder="Ingrese el nombre a buscar">
                    </div>
                    <div class="col-1 form-group">
                        <button type="submit" class="btn btn-secondary btn-block" id="btnSend">Buscar</button>
                    </div>
                </div>
            </form>
            <div class="d-flex justify-content-center d-none m-1" id="buttons">
                <a 
                    href="" 
                    onclick="event.preventDefault(); 
                    getPockemons(this.getAttribute('data-search'))" 
                    data-search="" 
                    class="btn btn-xs btn-outline-info m-1 disabled">< ant
                </a>
                <a 
                    href="" 
                    onclick="event.preventDefault(); 
                    getPockemons(this.getAttribute('data-search'))" 
                    data-search="" 
                    class="btn btn-xs btn-outline-info m-1">sig >
                </a>
            </div>
            <h5 id="title" class="mt-5 text-center">Resultados de la búsqueda</h5>
            <div id="content" class="d-flex justify-content-center m-5">
                <div class="" id="spinner">
                    <strong>Cargando...</strong>
                    <div class="spinner-border spinner-secondary ms-auto" role="status" aria-hidden="true"></div>
                  </div>
                <div class="d-none" id="list"></div>
            </div>
            <footer class="row fixed-bottom" style="background-color: white">
                <hr class="divider">
                <div class="col-6 p-3">
                    Hecho por: <a href="https://www.linkedin.com/in/leo-alvarez/" class="text-secondary btn-link">Leo Alvarez</a>
                </div>
                <div class="col-6">
                    <a href="https://gitlab.com/LeoMarAlvarez" class="btn btn-secondary btn-xs float-end m-3">Ir a mi Repo</a>
                </div>
            </footer>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
